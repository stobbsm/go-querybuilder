package querybuilder

import (
	"strings"
	"context"
	"database/sql"
	"regexp"
	"strconv"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
)

var db *sql.DB
var mock sqlmock.Sqlmock
var rows *sqlmock.Rows

const (
	postgresCreateTable = `CREATE TABLE users (id SERIAL PRIMARY KEY, name VARCHAR(100) NOT NULL, amount INT, cost REAL, active BOOLEAN NOT NULL DEFAULT true, created_at TIMESTAMP, updated_at TIMESTAMP)`
	postgresDropTable   = `DROP TABLE IF EXISTS users`

	postgresSelect = `SELECT name FROM users WHERE id = $1 AND amount = $2 OR cost = $3`
	postgresInsert = `INSERT INTO users (name, amount, cost) VALUES ($1, $2, $3)`
	postgresUpdate = `UPDATE users SET name = $1 WHERE id = $2`
	postgresDelete = `DELETE FROM users WHERE name = $1`

	postgresInnerJoin = `SELECT user.id, user.name, profile.background_id FROM user INNER JOIN profile ON user.profile_id = profile.id`

	postgresCreateTableWithConstraints = `CREATE TABLE profiles (id UUID PRIMARY KEY, email VARCHAR(100) UNIQUE, user_id UUID REFERENCES users(id), altered_at TIMESTAMP)`
)

func init() {
	var err error
	db, mock, err = sqlmock.New()
	if err != nil {
		panic(err)
	}

	rows = sqlmock.NewRows([]string{"id", "name", "amount", "cost"}).
		AddRow(1, "test", 2, 1.23).
		AddRow(2, "test2", 3, 4.44)
}

func TestCacheIsWorking(t *testing.T) {
	query := strings.TrimSpace( Prepare(Select("name"))(``) )
	expect := "SELECT name"
	if expect != query {
		t.Errorf("Expected:\n%s\nGot:\n%s\n", expect, query)
	}
}

func TestBuildValidSelectStatement(t *testing.T) {
	query := QueryBuilder(Select("name"), From("users"), WhereEq("id"), AndEq("amount"), OrEq("cost"))

	if postgresSelect != query {
		t.Errorf("Queries do not match!\nExpected:\n%s\nGot:\n%s\n", postgresSelect, query)
	}

	mock.ExpectPrepare(regexp.QuoteMeta(query)).
		ExpectQuery().
		WithArgs(1, 2, 1.23).
		WillReturnRows(rows)

	ctx := context.TODO()

	stmt, _ := db.PrepareContext(ctx, query)
	stmt.QueryContext(ctx, 1, 2, 1.23)

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Error(err)
	}
}

func BenchmarkBuildSelectWithOutCache(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = QueryBuilder(Select("name"), From("users"), WhereEq("id"), AndEq("amount"), OrEq("cost"))
	}
}

func BenchmarkBuildSelectWithCache(b *testing.B) {
	var selectName, fromUsers, whereID, andAmount, orCost FString
	selectName = Prepare(Select("name"))
	fromUsers = Prepare(From("users"))
	whereID = Prepare(WhereEq("id"))
	andAmount = Prepare(AndEq("amount"))
	orCost = Prepare(OrEq("cost"))
	for i := 0; i < b.N; i++ {
		_ = QueryBuilder(selectName, fromUsers, whereID, andAmount, orCost)
	}
}

func TestBuildValidInsertStatement(t *testing.T) {
	query := QueryBuilder(Insert("users", "name", "amount", "cost"))

	if postgresInsert != query {
		t.Errorf("Insert statements did not match\nExpected:\n%s\nGot:\n%s\n", postgresInsert, query)
	}

	mock.ExpectPrepare(regexp.QuoteMeta(query)).
		ExpectExec().
		WithArgs("Matt", 5, 2.25).
		WillReturnResult(sqlmock.NewResult(3, 1))

	ctx := context.TODO()
	stmt, _ := db.PrepareContext(ctx, query)
	stmt.ExecContext(ctx, "Matt", 5, 2.25)
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Error(err)
	}
}

func BenchmarkBuildInsertWithoutCache(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = QueryBuilder(Insert("users", "name", "amount", "cost"))
	}
}

func BenchmarkBuildInsertWithCache(b *testing.B) {
	var insertName FString
	insertName = Prepare(Insert("users", "name", "amount", "cost"))
	for i := 0; i < b.N; i++ {
		_ = QueryBuilder(insertName)
	}
}

func TestBuildValidUpdateStatement(t *testing.T) {
	var query = QueryBuilder(Update("users"), Set("name"), WhereEq("id"))

	if postgresUpdate != query {
		t.Errorf("Expected:\n%s\nGot:\n%s\n", postgresUpdate, query)
	}

	mock.ExpectPrepare(regexp.QuoteMeta(query)).
		ExpectExec().
		WithArgs("Matthew", 3).
		WillReturnResult(sqlmock.NewResult(3, 1))

	ctx := context.TODO()
	stmt, _ := db.PrepareContext(ctx, query)
	stmt.ExecContext(ctx, "Matthew", 3)
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Error(err)
	}
}

func BenchmarkBuildUpdateWithoutCache(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = QueryBuilder(Update("users"), Set("name"), WhereEq("id"))
	}
}

func BenchmarkBuildUpdateWithCache(b *testing.B) {
	var updateUsers, setName, whereID FString
	updateUsers = Prepare(Update("users"))
	setName = Prepare(Set("name"))
	whereID = Prepare(WhereEq("id"))
	for i := 0; i < b.N; i++ {
		_ = QueryBuilder(updateUsers, setName, whereID)
	}
}

func TestBuildValidDeleteStatement(t *testing.T) {
	var query = QueryBuilder(Delete, From("users"), WhereEq("name"))

	if postgresDelete != query {
		t.Errorf("Expected:\n%s\nGot:\n%s\n", postgresDelete, query)
	}

	mock.ExpectPrepare(regexp.QuoteMeta(query)).
		ExpectExec().
		WithArgs("Matthew").
		WillReturnResult(sqlmock.NewResult(3, 1))

	ctx := context.TODO()
	stmt, _ := db.PrepareContext(ctx, query)
	stmt.ExecContext(ctx, "Matthew")
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Error(err)
	}
}

func BenchmarkStrConvMethodNoAlloc(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = "$" + strconv.Itoa(i)
	}
}

func TestBuildValidCreateTableStatement(t *testing.T) {
	query := QueryBuilder(
		CreateTable("users"),
		WithColumns(
			PrimaryKey(Serial("id")),
			NotNull(VarChar("name", 100)),
			Int("amount"),
			Real("cost"),
			WithDefault(NotNull(Boolean("active")), "true"),
			TimeStamps(),
		),
	)
	if postgresCreateTable != query {
		t.Errorf("Insert statements did not match\nExpected:\n%s\nGot:\n%s\n", postgresCreateTable, query)
	}
}

func TestBuildValidDropTableStatement(t *testing.T) {
	query := QueryBuilder(
		DropTable("users"),
	)
	if postgresDropTable != query {
		t.Errorf("Insert statements did not match\nExpected:\n%s\nGot:\n%s\n", postgresDropTable, query)
	}
}

func TestBuildValidInnerJoin(t *testing.T) {
	query := QueryBuilder(
		Select("user.id", "user.name", "profile.background_id"),
		From("user"),
		InnerJoin("profile"),
		On("user.profile_id", "profile.id"),
	)
	if postgresInnerJoin != query {
		t.Errorf("Statements did not match\nExpected:\n%s\nGot:\n%s\n", postgresInnerJoin, query)
	}
}

func TestAlterTable(t *testing.T) {
	postgresAlterAddForeign := `ALTER TABLE profiles ADD FOREIGN KEY (user_id) REFERENCES users(id)`
	query := QueryBuilder(
		AlterTable(`profiles`),
		AddForeignKey(`user_id`, `id`, `users`),
	)
	if postgresAlterAddForeign != query {
		t.Errorf("Statements did not match\nExpected:\n%s\nGot:\n%s\n", postgresAlterAddForeign, query)
	}
}

func TestDropConstraint(t *testing.T) {
	postgresDropConstraint := `DROP CONSTRAINT profiles_user_id_fkey`
	query := QueryBuilder(
		DropConstraint(`profiles`, `user_id`, `fkey`),
	)
	if postgresDropConstraint != query {
		t.Errorf("Statements did not match\nExpected:\n%s\nGot:\n%s\n", postgresDropConstraint, query)
	}
}

func TestCreateTableWithConstraints(t *testing.T) {
	query := QueryBuilder(
		CreateTable(`profiles`),
		WithColumns(
			PrimaryKey(UUID()),
			Unique(VarChar(`email`, 100)),
			ForeignKey(`id`, `users`, Field(`user_id`, `UUID`)),
			TimeStamp(`altered_at`),
		),
	)
	if postgresCreateTableWithConstraints != query {
		t.Errorf("Statements did not match\nExpected:\n%s\nGot:\n%s\n", postgresCreateTableWithConstraints, query)
	}
}

func TestWhereInSub(t *testing.T) {
	postgresSelectWithSubQuery := `SELECT id, email, altered_at FROM profiles WHERE user_id IN (SELECT id FROM users WHERE id = $1)`
	query := QueryBuilder(
		Select(`id`, `email`, `altered_at`),
		From(`profiles`),
		WhereInSub(`user_id`,
			SubQuery(
				Select(`id`),
				From(`users`),
				WhereEq(`id`),
			),
		),
	)
	if postgresSelectWithSubQuery != query {
		t.Errorf("Statements did not match\nExpected:\n%s\nGot:\n%s\n", postgresSelectWithSubQuery, query)
	}
}