//
// Copyright (c) 2019 by Matthew Stobbs <matthew@stobbs.ca>. All Rights Reserved.
//

package querybuilder

// Update is functional Update
func Update(table string) FString {
	return func(s string) string {
		return "UPDATE " + table + " " + s
	}
}
