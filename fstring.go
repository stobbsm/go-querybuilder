//
// Copyright (c) 2019 by Matthew Stobbs <matthew@stobbs.ca>. All Rights Reserved.
//

package querybuilder

// FString is a special function type for function composition
type FString func(s string) string

// StringMap iterates over a slice of strings with a valid FString function type
func StringMap(fn FString, ss ...string) []string {
	var sn []string
	for _, v := range ss {
		sn = append(sn, fn(v))
	}
	return sn
}