package querybuilder

// AlterTable starts the Alter Table statement
func AlterTable(t string) FString {
	return func(s string) string {
		return "ALTER TABLE " + t + " " + s
	}
}

// AddForeignKey creates an AddForeignKey statement when altering a table
func AddForeignKey(name, refer, ontable string) FString {
	return func(s string) string {
		return "ADD FOREIGN KEY (" + name + ") REFERENCES " + ontable + "(" + refer + ")" + " " + s
	}
}

// DropConstraint assists in drop constraints based on the format { table }_{ field }_{ constrainttype }
// Example: DropConstraint("roles", "user_id", "fkey")
func DropConstraint(table, field, ctype string) FString {
	return func(s string) string {
		return "DROP CONSTRAINT " + table + "_" + field + "_" + ctype + " " + s
	}
}
