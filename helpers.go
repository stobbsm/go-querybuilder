//
// Copyright (c) 2019 by Matthew Stobbs <matthew@stobbs.ca>. All Rights Reserved.
//

package querybuilder

func argList(len int) string {
	var s string
	for i := 0; i < len; i++ {
		s = s + " ? "
		if i+1 != len {
			s = s + ", "
		}
	}
	return s
}