//
// Copyright (c) 2019 by Matthew Stobbs <matthew@stobbs.ca>. All Rights Reserved.
//

package querybuilder

import "strings"

// Insert is functional insert
func Insert(into string, columns ...string) FString {
	var values = argList(len(columns))
	var columnString = strings.Join(columns, ", ")
	return func(s string) string {
		return "INSERT INTO " + into + " (" + columnString + ") VALUES (" + values + ") " + s
	}
}
