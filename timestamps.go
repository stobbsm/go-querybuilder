package querybuilder

// TimeStamps inserts a created_at and updated_at timestamp
func TimeStamps() FString {
	return func(s string) string {
		return "created_at TIMESTAMP, updated_at TIMESTAMP" + s
	}
}