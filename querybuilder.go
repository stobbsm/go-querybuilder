//
// Copyright (c) 2019 by Matthew Stobbs <matthew@stobbs.ca>. All Rights Reserved.
//

package querybuilder

import (
	"strconv"
	"strings"
)

// Go Querybuilder, written for fun
// If you profit, great.

// QueryBuilder composes Or a functional query builder
// Example: QueryBuilder(Select("column1", "column2"), FTable("users"))
func QueryBuilder(fns ...FString) string {
	var res FString
	res = fns[0]
	for i := 1; i < len(fns); i++ {
		res = func(a FString, b FString) FString {
			return func(s string) string {
				return a(b(s))
			}
		}(res, fns[i])
	}
	var r = res(``)
	var numArgs = strings.Count(r, ` ? `)
	for i := 1; i <= numArgs; i++ {
		r = strings.Replace(r, " ? ", "$"+strconv.Itoa(i), 1)
	}
	return strings.TrimSpace(r)
}
