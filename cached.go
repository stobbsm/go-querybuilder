//
// Copyright (c) 2019 by Matthew Stobbs <matthew@stobbs.ca>. All Rights Reserved.
//

package querybuilder

// Prepare caches a generated string for reuse
func Prepare(fn FString) FString {
	var cache = make(map[string]string)
	return func(s string) string {
		if cache[s] == "" {
			cache[s] = fn(s)
		}
		return cache[s]
	}
}
