package querybuilder

import (
	"strconv"
	"strings"
)

// ColumnMutator is a function type that defines a Mutator.
// In this case, Mutator is more akin to PRIMARY KEY and NOT NULL.
type ColumnMutator func(fn FString) FString

// CreateTable begins a new create table query
func CreateTable(name string) FString {
	return func(s string) string {
		return "CREATE TABLE " + name + " (" + s + ")"
	}
}

// WithColumns builds columns through function composition
func WithColumns(fns ...FString) FString {
	var lines []string
	for _, fn := range fns {
		lines = append(lines, fn(""))
	}
	return func(s string) string {
		return strings.Join(lines, ", ") + s
	}
}

// WithDefault adds a default value. Value must always be as a string.
func WithDefault(fn FString, v string) FString {
	return CreateConstraint("DEFAULT "+v, fn)
}

// PrimaryKey is a mutator that creates a primary key from a given string
func PrimaryKey(fn FString) FString {
	return CreateConstraint("PRIMARY KEY", fn)
}

// NotNull sets a field to Not Null
func NotNull(fn FString) FString {
	return CreateConstraint("NOT NULL", fn)
}

// Unique sets a field to be Unique
func Unique(fn FString) FString {
	return CreateConstraint("UNIQUE", fn)
}

// ForeignKey helps in defining foreignkey constraints
func ForeignKey(refer, ontable string, fn FString) FString {
	return CreateConstraint("REFERENCES "+ontable+"("+refer+")", fn)
}

// CreateConstraint adds a constraint to the end of a line
func CreateConstraint(c string, fn FString) FString {
	return func(s string) string {
		return fn(s) + " " + c
	}
}

// Field creates a field with a given type
func Field(name, field string) FString {
	return func(s string) string {
		return name + " " + field + s
	}
}

// Boolean creates a boolean type column
func Boolean(name string) FString {
	var c = "BOOLEAN"
	return func(s string) string {
		return Field(name, c)(s)
	}
}

// Int creates a new serial type column
func Int(name string) FString {
	var c = "INT"
	return func(s string) string {
		return Field(name, c)(s)
	}
}

// Real creates a new serial type column
func Real(name string) FString {
	var c = "REAL"
	return func(s string) string {
		return Field(name, c)(s)
	}
}

// Serial creates a new serial type column
func Serial(name string) FString {
	var c = "SERIAL"
	return func(s string) string {
		return Field(name, c)(s)
	}
}

// VarChar creates a varchar column of  a given size
func VarChar(name string, size int) FString {
	var vsize = strconv.Itoa(size)
	var c = "VARCHAR(" + vsize + ")"
	return func(s string) string {
		return Field(name, c)(s)
	}
}

// TimeStamp inserts a timestamp column of the given name
func TimeStamp(name string) FString {
	var c = "TIMESTAMP"
	return func(s string) string {
		return Field(name, c)(s)
	}
}
