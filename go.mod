module gitlab.com/stobbsm/go-querybuilder

go 1.12

require (
	github.com/DATA-DOG/go-sqlmock v1.3.3
	github.com/bxcodec/faker v2.0.1+incompatible
	github.com/bxcodec/faker/v3 v3.1.0 // indirect
	github.com/kisielk/gotool v1.0.0 // indirect
	github.com/lib/pq v1.2.0 // indirect
	github.com/mdempsky/unconvert v0.0.0-20190325185700-2f5dc3378ed3 // indirect
	github.com/walle/lll v1.0.1 // indirect
	mvdan.cc/interfacer v0.0.0-20180901003855-c20040233aed // indirect
	mvdan.cc/lint v0.0.0-20170908181259-adc824a0674b // indirect
)
