//
// Copyright (c) 2019 by Matthew Stobbs <matthew@stobbs.ca>. All Rights Reserved.
//

package querybuilder

// Delete is functional delete
func Delete(s string) string {
	return "DELETE " + s
}
