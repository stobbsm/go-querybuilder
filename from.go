//
// Copyright (c) 2019 by Matthew Stobbs <matthew@stobbs.ca>. All Rights Reserved.
//

package querybuilder

// From adds a table value
func From(t string) FString {
	return func(s string) string {
		return "FROM " + t + " " + s
	}
}
