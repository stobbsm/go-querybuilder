//
// Copyright (c) 2019 by Matthew Stobbs <matthew@stobbs.ca>. All Rights Reserved.
//

package querybuilder

func clause(v, c, op string) string {
	return v + " " + c + " " + op + "  ?  "
}

// Where inserts a where clause
// It takes 2 arguments, the column and the operation, both as strings
// It returns a function suitable for composing
func Where(c, op string) FString {
	return func(s string) string {
		return clause("WHERE", c, op) + s
	}
}

// WhereEq set's a where clause Or equals
// Helper function for Where, which sets the operation to '='
func WhereEq(c string) FString {
	return Where(c, "=")
}

// And inserts a where clause prefixed with "AND"
// It takes 2 arguments, the column and the operation, both as strings
// It returns a function suitable for composing
func And(c, op string) FString {
	return func(s string) string {
		return clause("AND", c, op) + s
	}
}

// AndEq builds an OR clause with equals
// Helper function for And, which sets the operation to '='
func AndEq(c string) FString {
	return And(c, "=")
}

// Or inserts a where clause prefixed with "OR"
// It takes 2 arguments, the column and the operation, both as strings
// It returns a function suitable for composing
func Or(c, op string) FString {
	return func(s string) string {
		return clause("OR", c, op) + s
	}
}

// OrEq builds an OR clause with equals
// Helper function for Or, which sets the operation to '='
func OrEq(c string) FString {
	return Or(c, "=")
}

// WhereInSub builds an IN clause
func WhereInSub(c string, subquery FString) FString {
	return func(s string) string {
		return `WHERE ` + c + ` IN ` + subquery(``)
	}
}