package querybuilder

import "strings"

// InnerJoin assists in creating an inner join query
func InnerJoin(t string) FString {
	return func(s string) string {
		return "INNER JOIN " + t + " " + s
	}
}

// On sets the constraints for Joins
func On(c1, c2 string) FString {
	return func(s string) string {
		return "ON " + c1 + " = " + c2 + " " + s
	}
}

// SubQuery inserts a subquery
func SubQuery(fns ...FString) FString {
	var res FString
	res = fns[0]
	for i := 1; i < len(fns); i++ {
		res = func(a FString, b FString) FString {
			return func(s string) string {
				return a(b(s))
			}
		}(res, fns[i])
	}
	r := strings.TrimSpace(res(``))
	return func(s string) string {
		return `(` + r + ` )` + s
	}
}
