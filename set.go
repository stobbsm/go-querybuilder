//
// Copyright (c) 2019 by Matthew Stobbs <matthew@stobbs.ca>. All Rights Reserved.
//

package querybuilder

import "strings"

// Set builds a "set" clause
func Set(columns ...string) FString {
	var sets = strings.Join(
		StringMap(
			func(s string) string {
				return s + " =  ? "
			},
			columns...,
		),
		", ",
	)
	return func(s string) string {
		return "SET " + sets + " " + s
	}
}
