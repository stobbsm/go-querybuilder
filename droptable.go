package querybuilder

// DropTable builds a drop table query
func DropTable(t string) FString {
	return func(s string) string {
		return "DROP TABLE IF EXISTS " + t + s
	}
}
