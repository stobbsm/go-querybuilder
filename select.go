//
// Copyright (c) 2019 by Matthew Stobbs <matthew@stobbs.ca>. All Rights Reserved.
//

package querybuilder

import "strings"

// Select is functional select
func Select(columns ...string) FString {
	var c string
	c = strings.Join(columns, ", ")
	return func(s string) string {
		return "SELECT " + c + " " + s
	}
}
