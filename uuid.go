package querybuilder

// UUID create's a field called id with the UUID type
func UUID() FString {
	return func(s string) string {
		return Field("id", "UUID")(s)
	}
}
